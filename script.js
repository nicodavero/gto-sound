let tracks = [

    { title: 'Boys Got to Go', artist: 'BRONCHO', url: './media/Boys Got to Go.mp3', cover: './media/af57b34b4d68150a6ba1408a81b53f62.600x600x1.jpg' },

    { title: 'Standing Next to Me', artist: 'The Last Shadow Puppet', url: './media/The Last Shadow Puppets - standing next to me.mp3', cover: './media/220px-LastShadowPuppetsStanding.jpg' },

    { title: 'You Seemed so Happy', artist: 'The Japanese House', url: './media/You Seemed so Happy.mp3', cover: './media/71YZ13EYpuL._SL1200_.jpg' },

]

let track = document.querySelector('#track')
let btnPlay = document.querySelector('#btn-play')
let btnPause = document.querySelector('#btn-pause')
let btnPrev = document.querySelector('#btn-prev')
let btnNext = document.querySelector('#btn-next')
let playing = false
let currentTrack = 0
let currentTime = document.querySelector('#current-time')
let totalTime = document.querySelector('#total-time')

function play() {

    if (!playing) {

        btnPlay.classList.add('d-none')
        btnPause.classList.remove('d-none')

        track.play()

        playing = true

    } else {

        btnPlay.classList.remove('d-none')

        btnPause.classList.add('d-none')

        track.pause()

        playing = false

    }

}

function nextTrack() {

    currentTrack++

    if (currentTrack > tracks.length - 1) {

        currentTrack = 0

    }

    track.src = tracks[currentTrack].url

    trackTitle.innerHTML = truncate(tracks[currentTrack].title, 14)

    artistName.innerHTML = truncate(tracks[currentTrack].artist, 11)

    coverImg.src = tracks[currentTrack].cover

    if (playing) {

        playing = false
        play()

    }
}

function prevTrack() {

    currentTrack--

    if (currentTrack < 0) {

        currentTrack = tracks.length - 1

    }

    track.src = tracks[currentTrack].url
    trackTitle.innerHTML = truncate(tracks[currentTrack].title, 14)
    artistName.innerHTML = truncate(tracks[currentTrack].artist, 11)
    coverImg.src = tracks[currentTrack].cover

    if (playing) {

        playing = false
        play()

    }

}

function truncate(str, n) {
    return (str.length > n) ? str.substr(0, n - 1) + '...' : str;
}

setInterval(function () {

    currentTime.innerHTML = formatTime(track.currentTime);

    totalTime.innerHTML = formatTime(track.duration);

}, 1000)

function formatTime(sec) {

    let minutes = Math.floor(sec / 60);

    let seconds = Math.floor(sec - minutes * 60);

    if (seconds < 10) {

        seconds = `0${seconds}`;

    }

    return `${minutes}.${seconds}`

}

let artistName = document.querySelector('#artist-name')
let trackTitle = document.querySelector('#track-title')
let coverImg = document.querySelector('#cover-img')

btnPlay.addEventListener('click', play)
btnPause.addEventListener('click', play)
btnNext.addEventListener('click', nextTrack)
btnPrev.addEventListener('click', prevTrack)
track.addEventListener('ended', nextTrack)